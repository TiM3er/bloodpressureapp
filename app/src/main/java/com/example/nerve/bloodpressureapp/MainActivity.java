package com.example.nerve.bloodpressureapp;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.Toast;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class MainActivity extends AppCompatActivity {
    Button timePickerButton, dataPickerButton;
    public static Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void onClickSendBloodPressure(View view) {
        String wysilek = "";
        EditText editTextSystiolic = (EditText) findViewById(R.id.systiolic);
        EditText editTextDiastiloc = (EditText) findViewById(R.id.diastiloc);
        EditText editTextPulse = (EditText) findViewById(R.id.pulse);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(timestamp);
        Switch aSwitch = (Switch) findViewById(R.id.switchMedicine);
        SeekBar seekBar = (SeekBar) findViewById(R.id.effortSeekBar);
        if (seekBar.getProgress() == 0) {
            wysilek = "\"Maly\"";
        } else if (seekBar.getProgress() == 1) {
            wysilek = "\"Sredni\"";
        } else if (seekBar.getProgress() == 2) {
            wysilek = "\"Duzy\"";
        } else {
            wysilek = "\"Brak brak\"";
        }


        final String request = "{\"data\":\"" + timeStamp + "\"," +
                "\"systiolic\":" + editTextSystiolic.getText() + "," +
                "\"diastiloc\":" + editTextDiastiloc.getText() + "," +
                "\"pulse\":" + editTextPulse.getText() + "," +
                "\"medicine\":" + aSwitch.isChecked() + "," +
                "\"effort\":" + wysilek +
                "}";

        Log.e("Rest" , request);

        final String endPointURL = "http://80.211.34.226:8082/bloodPressureAdd";
        Request requestClass = new Request(request, endPointURL);
        System.out.println(timestamp);


        String respone = null;
        try {
//            myMLClient.requestResponse(request);
            if (requestClass.requestResponse()) {
                editTextSystiolic.setText("");
                editTextDiastiloc.setText("");
                editTextPulse.setText("");
                Toast.makeText(MainActivity.this,
                        "udało sie ", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(MainActivity.this,
                        "nie udało sie ", Toast.LENGTH_LONG).show();
            }


        } catch (Exception e) {
            e.printStackTrace();


        }

        Log.e("dane ", editTextSystiolic.getText() + " " + editTextDiastiloc.getText() + " " + editTextPulse.getText() + " ");
    }


}
