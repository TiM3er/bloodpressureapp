package com.example.nerve.bloodpressureapp;

import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Kostek11 on 2018-08-11.
 */

public class Request {
    String request;
    String endPointURL;

    public Request(String request, String endPointURL) {
        this.request = request;
        this.endPointURL = endPointURL;
    }

    public boolean requestResponse() throws Exception {


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        URL u = new URL(endPointURL);
        HttpURLConnection conn = (HttpURLConnection) u.openConnection();

        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        conn.connect();

        String body = new String(request);


        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

        wr.write(body);
        wr.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));


        System.out.println(in.toString());
        String integersInString = in.readLine();
        System.out.println(integersInString);
        if (integersInString.equals("true")) {
           return true;
        } else {
          return false;

        }


    }
}
